// **Iteración #4: Map**

/* 4.1 Dado el siguiente array, devuelve un array con sus nombres 
utilizando .map(). */
const users = [
    { id: 1, name: 'Abel' },
    { id: 2, name: 'Julia' },
    { id: 3, name: 'Pedro' },
    { id: 4, name: 'Amanda' }
];

const allNames = users.map(user => ({
    nombre: user.name
}));

console.log(allNames);

/* 4.2 Dado el siguiente array, devuelve una lista que contenga los valores 
de la propiedad .name y cambia el nombre a 'Anacleto' en caso de que 
empiece por 'A'. */
const users2 = [
    { id: 1, name: 'Abel' },
    { id: 2, name: 'Julia' },
    { id: 3, name: 'Pedro' },
    { id: 4, name: 'Amanda' }
];

const namesAll = users2.map(user => ({
    name: user.name
}));

console.log('Sin cambiar')
console.log(namesAll)

Object.entries(namesAll).forEach(name => {
    if (name[1].name.startsWith('A')) {
        // console.log(name[1].name + ' change');
        name[1].name = 'Anacleto'

    } else {
        name[1].name = name[1].name
    }
    return name[1].name
});
console.log('Cambiado')
console.log(namesAll)

/* 4.3 Dado el siguiente array, devuelve una lista que contenga los valores 
de la propiedad .name y añade al valor de .name el string ' (Visitado)' 
cuando el valor de la propiedad isVisited = true. */
const cities = [
    { isVisited: true, name: 'Tokyo' },
    { isVisited: false, name: 'Madagascar' },
    { isVisited: true, name: 'Amsterdam' },
    { isVisited: false, name: 'Seul' }
];

var allCities = cities.map((city) => {
    return city.name
});

console.log(allCities)

cities.reduce(function (accumulator, city) {
    // console.log(city)
    if (city.isVisited === true) {
        console.log(city.name + ' Visitado')
    }
    else {
        console.log(city.name)
    }
}, '');
